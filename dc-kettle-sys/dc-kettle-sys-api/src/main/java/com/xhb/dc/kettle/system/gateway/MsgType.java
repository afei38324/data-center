package com.xhb.dc.kettle.system.gateway;

/**
 * 消息类型.
 */
public enum MsgType {
    /**
     * 新增或修改.
     */
    UPSET,
    /**
     * 删除.
     */
    DELETE
}
