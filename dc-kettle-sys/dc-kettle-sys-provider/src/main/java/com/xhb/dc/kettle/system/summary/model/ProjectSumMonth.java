package com.xhb.dc.kettle.system.summary.model;

import lombok.Data;

/**
 * ProjectSumMonth.
 */
@Data
public class ProjectSumMonth {
    private int cnt;

    private String mon;

    private String projectType;

}
