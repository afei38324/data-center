package com.xhb.dc.kettle.portal.model.datasource.dto;

import lombok.Data;

/**
 * UserListDTO.
 */
@Data
public class UserListDTO {
    private String userName;

    private String userId;

}
